import * as React from 'react';
import {Button, View} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import Home from './screens/home';
import About from './screens/about';
import Contact from './screens/contact';
import investorLogin from './screens/investorLogin';
import CommercialLeasing from './screens/commercialLeasing';
import CommercialLeasingFilter from './screens/CommercialLeasingFilter';
import CommercialLeasingPropertyDetail from './screens/CommercialLeasingPropertyDetails';
import InvestmentOptions from './screens/InvestmentOptions';
import InvestmentOptionsFilter from './screens/InvestmentOptionsFilter';
import InvestmentOptionsPropertyDetail from './screens/InvestmentOptionsPropertyDetails';
import FullBuyFAQ from './screens/FullBuyFAQ';
import Web3 from './screens/Web3.0';
import FractionInvestmentFAQ from './screens/FractionalInvestmentFAQ';
import AboutApp from './screens/AboutApp';
import ResidentialBuying from './screens/ResidentialBuying';
import ResidentialBuyingPropertyDetail from './screens/ResidentialBuyingPropertyDetail';
import ResidentialBuyingFilter from './screens/ResidentialBuyingFilter';
import ResidentialRenting from './screens/ResidentialRenting';
import ResidentialRentingFilter from './screens/ResidentialRentingFilter';
import ResidentialRentingPropertyDetail from './screens/ResidentialRentingPropertyDetail';
import {globalStyles} from './styles/global';

const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen name="Home" component={Home} />
        <Drawer.Screen name="About Us" component={About} />
        <Drawer.Screen name="Contact" component={Contact} />
        <Drawer.Screen
          name="Commercial Leasing"
          component={CommercialLeasing}
        />
        <Drawer.Screen
          name="Investment Options"
          component={InvestmentOptions}
        />
        <Drawer.Screen
          name="Residential Renting"
          component={ResidentialRenting}
        />
        <Drawer.Screen
          name="Residential Buying"
          component={ResidentialBuying}
        />
        <Drawer.Screen
          name="Residential Buying filter"
          component={ResidentialBuyingFilter}
          options={{
            drawerItemStyle: {display: 'none'},
          }}
        />
        <Drawer.Screen name="Full Buy FAQ" component={FullBuyFAQ} />
        <Drawer.Screen
          name="Fractional Investment FAQ"
          component={FractionInvestmentFAQ}
        />
        <Drawer.Screen name="Web 3.0" component={Web3} />

        <Drawer.Screen name="Investor Login" component={investorLogin} />

        <Drawer.Screen name="About App" component={AboutApp} />
        <Drawer.Screen
          name="Residential Buying Property Detail"
          component={ResidentialBuyingPropertyDetail}
          options={{
            drawerItemStyle: {display: 'none'},
          }}
        />
        <Drawer.Screen
          name="Commercial Leasing Property Detail"
          component={CommercialLeasingPropertyDetail}
          options={{
            drawerItemStyle: {display: 'none'},
          }}
        />
        <Drawer.Screen
          name="Commercial Leasing Filter"
          component={CommercialLeasingFilter}
          options={{
            drawerItemStyle: {display: 'none'},
          }}
        />
        <Drawer.Screen
          name="Investment Options Filter"
          component={InvestmentOptionsFilter}
          options={{
            drawerItemStyle: {display: 'none'},
          }}
        />
        <Drawer.Screen
          name="Investment Options Property Detail"
          component={InvestmentOptionsPropertyDetail}
          options={{
            drawerItemStyle: {display: 'none'},
          }}
        />
        <Drawer.Screen
          name="Residential Renting Property Detail"
          component={ResidentialRentingPropertyDetail}
          options={{
            drawerItemStyle: {display: 'none'},
          }}
        />
        <Drawer.Screen
          name="Residential Renting filter"
          component={ResidentialRentingFilter}
          options={{
            drawerItemStyle: {display: 'none'},
          }}
        />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}
