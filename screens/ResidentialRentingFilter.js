import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Button,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  Modal,
  Alert,
  Pressable,
  TouchableHighlight,
} from 'react-native';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import {Picker} from '@react-native-picker/picker';
import Slider from '@react-native-community/slider';
import Icon from 'react-native-vector-icons/Ionicons';
import MultiSlider from '@ptomasroos/react-native-multi-slider';

import {globalStyles} from '../styles/global';
import {validatePathConfig} from '@react-navigation/native';
import FontAwesome, {
  SolidIcons,
  RegularIcons,
  BrandIcons,
  parseIconFromClassName,
} from 'react-native-fontawesome';

export default class ResidentialRentingFilter extends React.Component {
  constructor() {
    super();
    this.state = {
      investmentrange: 350,
      statename: '',
      propertytype: '',
      modalVisible: false,
      communicate: '',
      dataset: [],
      name: '',
      email: '',
      phonenumber: '',
      minBudget: '',
      maxBudget: '',
      areaType: '',
      minArea: '',
      maxArea: '',
      minFloor: '',
      maxFloor: '',
      state: '',
      city: '',
      location: '',
      stateList: [],
      cityList: [],
      locationList: [],
      OneBhkBackgroundColor: 'white',
      TwoBhkBackgroundColor: 'white',
      ThreeBhkBackgroundColor: 'white',
      FourBhkBackgroundColor: 'white',
      NorthBackgroundColor: 'white',
      SouthBackgroundColor: 'white',
      EastBackgroundColor: 'white',
      WestBackgroundColor: 'white',
      FurnishedBackgroundColor: 'white',
      UnfurnishedBackgroundColor: 'white',
      SemiUnfurnishedBackgroundColor: 'white',
      GatedBackgroundColor: 'white',
      StandaloneBackgroundColor: 'white',
      FlatbackgroundColor: 'white',
      HousebackgroundColor: 'white',
      rentRange: [1, 5000],
      areaRange: [1, 5000],
    };
  }
  multiSliderrentRangeChange = rentRange => {
    this.setState({
      rentRange,
    });
  };
  multiSliderareaRangeChange = areaRange => {
    this.setState({
      areaRange,
    });
  };
  changeOneBhkColor() {
    this.state.pressed
      ? this.setState({pressed: false, OneBhkBackgroundColor: 'white'})
      : this.setState({pressed: true, OneBhkBackgroundColor: '#ffc451'});
  }
  changeTwoBhkColor() {
    this.state.pressed
      ? this.setState({pressed: false, TwoBhkBackgroundColor: 'white'})
      : this.setState({pressed: true, TwoBhkBackgroundColor: '#ffc451'});
  }
  changeThreeBhkColor() {
    this.state.pressed
      ? this.setState({pressed: false, ThreeBhkBackgroundColor: 'white'})
      : this.setState({pressed: true, ThreeBhkBackgroundColor: '#ffc451'});
  }
  changeFourBhkColor() {
    this.state.pressed
      ? this.setState({pressed: false, FourBhkBackgroundColor: 'white'})
      : this.setState({pressed: true, FourBhkBackgroundColor: '#ffc451'});
  }
  changeNorthColor() {
    this.state.pressed
      ? this.setState({pressed: false, NorthBackgroundColor: 'white'})
      : this.setState({pressed: true, NorthBackgroundColor: '#ffc451'});
  }
  changeSouthColor() {
    this.state.pressed
      ? this.setState({pressed: false, SouthBackgroundColor: 'white'})
      : this.setState({pressed: true, SouthBackgroundColor: '#ffc451'});
  }
  changeEastColor() {
    this.state.pressed
      ? this.setState({pressed: false, EastBackgroundColor: 'white'})
      : this.setState({pressed: true, EastBackgroundColor: '#ffc451'});
  }
  changeWestColor() {
    this.state.pressed
      ? this.setState({pressed: false, WestBackgroundColor: 'white'})
      : this.setState({pressed: true, WestBackgroundColor: '#ffc451'});
  }
  changeFurnishedColor() {
    this.state.pressed
      ? this.setState({pressed: false, FurnishedBackgroundColor: 'white'})
      : this.setState({pressed: true, FurnishedBackgroundColor: '#ffc451'});
  }
  changeUnfurnishedColor() {
    this.state.pressed
      ? this.setState({pressed: false, UnfurnishedBackgroundColor: 'white'})
      : this.setState({pressed: true, UnfurnishedBackgroundColor: '#ffc451'});
  }
  changeSemiUnfurnishedColor() {
    this.state.pressed
      ? this.setState({pressed: false, SemiUnfurnishedBackgroundColor: 'white'})
      : this.setState({
          pressed: true,
          SemiUnfurnishedBackgroundColor: '#ffc451',
        });
  }
  changeGatedColor() {
    this.state.pressed
      ? this.setState({pressed: false, GatedBackgroundColor: 'white'})
      : this.setState({pressed: true, GatedBackgroundColor: '#ffc451'});
  }
  changeStandaloneColor() {
    this.state.pressed
      ? this.setState({pressed: false, StandaloneBackgroundColor: 'white'})
      : this.setState({pressed: true, StandaloneBackgroundColor: '#ffc451'});
  }
  changeFlatColor() {
    this.state.pressed
      ? this.setState({pressed: false, FlatbackgroundColor: 'white'})
      : this.setState({pressed: true, FlatbackgroundColor: '#ffc451'});
  }
  changeHouseColor() {
    this.state.pressed
      ? this.setState({pressed: false, HousebackgroundColor: 'white'})
      : this.setState({pressed: true, HousebackgroundColor: '#ffc451'});
  }
  updateValue(text, field) {
    if (field == 'name') {
      this.setState({
        name: text,
      });
    } else if (field == 'email') {
      this.setState({
        email: text,
      });
    } else if (field == 'phonenumber') {
      this.setState({
        phonenumber: text,
      });
    } else if (field == 'message') {
      this.setState({
        message: text,
      });
    }
  }
  submit() {
    let collection = {};
    (collection.name = this.state.name),
      (collection.email = this.state.email),
      (collection.phonenumber = this.state.phonenumber),
      (collection.enquirytype = this.state.communicate);

    var url = 'https://www.whitegrasscapital.com/api/contactmail';

    fetch(url, {
      method: 'POST', // or 'PUT'
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(collection),
    })
      .then(response => response.json())
      .then(response => {
        console.log('Success:', response);
      })
      .catch(error => {
        console.error('Error:', error);
      });
    Alert.alert('Success!', 'We will get back to you.');
  }
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  showProperties() {
    this.props.navigation.navigate('Residential Renting');
  }

  componentDidMount() {
    return fetch('https://www.whitegrasscapital.com/Api/get_state', {
      method: 'POST',
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          stateList: responseJson.data.states,
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  onSateValueChange = (itemValue, itemPosition) => {
    if (itemValue == '') {
      Alert.alert('Please select state.');
    } else {
      this.setState({state: itemValue});
      return fetch(
        'https://www.whitegrasscapital.com/Api/get_cities?state_id=' +
          itemValue,
        {
          method: 'POST',
        },
      )
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            cityList: responseJson.data.city,
          });
        })
        .catch(error => {
          console.error(error);
        });
    }
  };
  onCityValueChange = (itemValue, itemPosition) => {
    if (itemValue == '') {
      Alert.alert('Please select city.');
    } else {
      this.setState({city: itemValue});
      return fetch(
        'https://www.whitegrasscapital.com/Api/get_location?city_id=' +
          itemValue,
        {
          method: 'POST',
        },
      )
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            locationList: responseJson.data.locationdata,
          });
        })
        .catch(error => {
          console.error(error);
        });
    }
  };

  render() {
    return (
      <ScrollView>
        {/* New Filter Design Starts */}

        <View style={globalStyles.NewFilterLayout}>
          <View style={globalStyles.NewFilterLayout}>
            <Text style={globalStyles.FilterLabels}>State</Text>

            <View style={globalStyles.BudgetDropdownLayout}>
              <View style={globalStyles.AreaDropdown}>
                <Picker
                  selectedValue={this.state.state}
                  onValueChange={(itemValue, itemIndex) => {
                    this.onSateValueChange(itemValue);
                  }}>
                  <Picker.Item label="State" value="" />
                  {this.state.stateList.map(obj => (
                    <Picker.Item label={obj.name} value={obj.id} />
                  ))}
                </Picker>
              </View>
            </View>
            <Text style={globalStyles.FilterLabels}>City</Text>
            <View style={globalStyles.AreaDropdown}>
              <Picker
                selectedValue={this.state.city}
                onValueChange={(itemValue, itemIndex) => {
                  this.onCityValueChange(itemValue);
                }}>
                <Picker.Item label="City" value="" />
                {this.state.cityList.map(obj => (
                  <Picker.Item label={obj.name} value={obj.id} />
                ))}
              </Picker>
            </View>

            <Text style={globalStyles.FilterLabels}>Location</Text>

            <View style={globalStyles.AreaDropdown}>
              <Picker
                selectedValue={this.state.location}
                onValueChange={(itemValue, itemIndex) => {
                  if (itemValue == '') {
                    Alert.alert('Please select location.');
                  } else {
                    this.setState({location: itemValue});
                  }
                }}>
                <Picker.Item label="Location" value="" />
                {this.state.locationList.map(obj => (
                  <Picker.Item label={obj.name} value={obj.id} />
                ))}
              </Picker>
            </View>
          </View>
          <View style={globalStyles.NewFilterLayout}>
            <Text style={globalStyles.FilterLabels}>Area</Text>
            <View style={{marginLeft: 25}}>
              <MultiSlider
                style={{width: 350, marginLeft: 15}}
                values={[this.state.areaRange[0], this.state.areaRange[1]]}
                sliderLength={275}
                onValuesChange={this.multiSliderareaRangeChange}
                min={1}
                max={5000}
                step={1}
                thumbTintColor="rgb(252, 228, 149)"
                thumbTintSize="30"
              />
            </View>
            <View style={globalStyles.MinMaxContainer}>
              <View style={globalStyles.item}>
                <Text style={globalStyles.MinMaxFilterLabels}>
                  Min : {this.state.areaRange[0]}
                </Text>
              </View>
              <View style={globalStyles.item}>
                <Text style={globalStyles.MinMaxFilterLabels}>
                  Max : {this.state.areaRange[1]}
                </Text>
              </View>
            </View>
          </View>

          <View style={globalStyles.NewFilterLayout}>
            <Text style={globalStyles.FilterLabels}>Rent</Text>
            <View style={{marginLeft: 25}}>
              <MultiSlider
                style={{width: 350, marginLeft: 15}}
                values={[this.state.rentRange[0], this.state.rentRange[1]]}
                sliderLength={275}
                onValuesChange={this.multiSliderrentRangeChange}
                min={1}
                max={5000}
                step={1}
                thumbTintColor="rgb(252, 228, 149)"
                thumbTintSize="30"
              />
            </View>
            <View style={globalStyles.MinMaxContainer}>
              <View style={globalStyles.item}>
                <Text style={globalStyles.MinMaxFilterLabels}>
                  Min : {this.state.rentRange[0]}
                </Text>
              </View>
              <View style={globalStyles.item}>
                <Text style={globalStyles.MinMaxFilterLabels}>
                  Max : {this.state.rentRange[1]}
                </Text>
              </View>
            </View>
          </View>
          <View style={globalStyles.NewFilterLayout}>
            <Text style={globalStyles.FilterLabels}>Bedrooms</Text>
            <ScrollView
              style={globalStyles.PropertyTypeIconLayout}
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              <TouchableOpacity
                style={{
                  backgroundColor: this.state.OneBhkBackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeOneBhkColor()}>
                <View>
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    1 BHK
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: this.state.TwoBhkBackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeTwoBhkColor()}>
                <View>
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    2 BHK
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={{
                  backgroundColor: this.state.ThreeBhkBackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeThreeBhkColor()}>
                <View>
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    3 BHK
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={{
                  backgroundColor: this.state.FourBhkBackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeFourBhkColor()}>
                <View>
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    4 BHK
                  </Text>
                </View>
              </TouchableOpacity>
            </ScrollView>
          </View>
          <View style={globalStyles.NewFilterLayout}>
            <Text style={globalStyles.FilterLabels}>Facing</Text>
            <ScrollView
              style={globalStyles.PropertyTypeIconLayout}
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              <TouchableOpacity
                style={{
                  backgroundColor: this.state.NorthBackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeNorthColor()}>
                <View>
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    North
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: this.state.SouthBackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeSouthColor()}>
                <View>
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    South
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: this.state.EastBackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeEastColor()}>
                <View>
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    East
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: this.state.WestBackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeWestColor()}>
                <View>
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    West
                  </Text>
                </View>
              </TouchableOpacity>
            </ScrollView>
          </View>
          <View style={globalStyles.NewFilterLayout}>
            <Text style={globalStyles.FilterLabels}>Status</Text>
            <View style={globalStyles.BudgetDropdownLayout}>
              <TouchableOpacity
                style={{
                  backgroundColor: this.state.FurnishedBackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeFurnishedColor()}>
                <View>
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    Furnished
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: this.state.UnfurnishedBackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeUnfurnishedColor()}>
                <View>
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    Unfurnished
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: this.state.SemiUnfurnishedBackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeSemiUnfurnishedColor()}>
                <View>
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    Semi Unfurnished
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <View style={globalStyles.NewFilterLayout}>
            <Text style={globalStyles.FilterLabels}>Society Type</Text>
            <View style={globalStyles.BudgetDropdownLayout}>
              <TouchableOpacity
                style={{
                  backgroundColor: this.state.GatedBackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeGatedColor()}>
                <View>
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    Gated
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: this.state.StandaloneBackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeStandaloneColor()}>
                <View>
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    Standalone
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>

          <View style={globalStyles.NewFilterLayout}>
            <Text style={globalStyles.FilterLabels}>Type</Text>
            <ScrollView
              style={globalStyles.PropertyTypeIconLayout}
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              <TouchableOpacity
                style={{
                  backgroundColor: this.state.FlatbackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeFlatColor()}>
                <View>
                  <FontAwesome
                    style={globalStyles.iconStyle}
                    icon={SolidIcons.home}
                  />
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    Flat
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: this.state.HousebackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeHouseColor()}>
                <View>
                  <FontAwesome
                    style={globalStyles.iconStyle}
                    icon={SolidIcons.building}
                  />
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    House
                  </Text>
                </View>
              </TouchableOpacity>
            </ScrollView>
          </View>
 <TouchableOpacity onPress={() => this.showProperties()}>
            <Text style={globalStyles.showPropertiesButton}>
              Show Properties
            </Text>
          </TouchableOpacity>
        </View>
        {/* New Filter Design Ends */}
      </ScrollView>
    );
  }
}
