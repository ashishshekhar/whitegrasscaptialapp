import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Button,
  TouchableOpacity,
  ImageBackground,
  TextInput,
} from 'react-native';

import {globalStyles} from '../styles/global';

export default class About extends React.Component {
  render() {
    return (
      <ScrollView>
        {/* Start About Section */}

        <View style={globalStyles.aboutSection}>
          <ImageBackground
            source={require('/Users/ashish/Projects/whitegrass/WhiteGrass/images/yellow-scratch.png')}
            style={globalStyles.aboutSectionBGImage}>
            <Text style={globalStyles.aboutHeadText}>About Us</Text>
          </ImageBackground>

          <Text style={globalStyles.aboutDetails}>
            At White Grass Capital, we aim to give you a transparent, tech and
            data driven, end to end service and solution for all your Real
            Estate Investment needs so that it gives you can get the highest
            return possible.
          </Text>
          <Text style={globalStyles.aboutSubDetails}>
            Our specialised team focus on how to create maximum value on your
            Real Estate Investments. Our tech driven approach, virtual
            integration and end to end services makes sure you get peace of
            mind. Through our various products, we aim to provide a level
            playing field for all our small and big investors. Through our data
            analyses, regular feedbacks, and ease of use, you can customise and
            diversify your investments as it suits you. We have specialised team
            for NRIs, HNIs &amp; our Corporate customers.
          </Text>
          <TouchableOpacity>
            <Text style={globalStyles.getInTouchButton}>Get In Touch</Text>
          </TouchableOpacity>
        </View>

        {/* End About Section */}
      </ScrollView>
    );
  }
}
