import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Button,
  TouchableOpacity,
  ImageBackground,
  TextInput,
} from 'react-native';

import {globalStyles} from '../styles/global';
import Icon from 'react-native-vector-icons/Ionicons';
import * as Animatable from 'react-native-animatable';
import CarouselCards from '../CarouselCards';
import CarouselNewsCards from '../CarouselNewsCards';
import {Picker} from '@react-native-picker/picker';

export default class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      name: '',
      email: '',
      phonenumber: '',
      message: '',
      enquiryType: '',
    };
  }
  updateValue(text, field) {
    if (field == 'name') {
      this.setState({
        name: text,
      });
    } else if (field == 'email') {
      this.setState({
        email: text,
      });
    } else if (field == 'phonenumber') {
      this.setState({
        phonenumber: text,
      });
    } else if (field == 'message') {
      this.setState({
        message: text,
      });
    }
  }
  submit() {
    let collection = {};
    (collection.name = this.state.name),
      (collection.email = this.state.email),
      (collection.phonenumber = this.state.phonenumber),
      (collection.enquiryType = this.state.enquiryType),
      (collection.message = this.state.message);
    console.warn(collection);
  }
  render() {
    return (
      <ScrollView>
        <View style={globalStyles.container}>
          <Text style={globalStyles.logoText}>White Grass Capital</Text>
          <Text style={globalStyles.detailText}>
            East India’s leading management Company on Real Estate
            <ImageBackground
              source={require('/Users/ashish/Projects/whitegrass/WhiteGrass/images/scratch.png')}
              style={globalStyles.aboutSectionBGImage}>
              <Text style={globalStyles.aboutHeadText}>Investment</Text>
            </ImageBackground>
          </Text>

          <Text style={globalStyles.detailSubText}>
            Safe, Reliable, Experienced Team to guide you through your real
            estate investment journey! Post your wish now!
          </Text>
          <Animatable.View
            style={globalStyles.cardSection}
            animation="slideInUp"
            iterationCount={'infinite'}
            direction="alternate">
            <View style={globalStyles.cardDesign}>
              <View style={globalStyles.cardImage}>
                <Image
                  source={{
                    uri: 'https://www.whitegrasscapital.com/assets/img/zero_invest.jpg',
                  }}
                  style={{
                    width: 200,
                    height: 200,
                  }}
                />
              </View>
              <Text style={globalStyles.cardHeader}>Zero Brokerage on buy</Text>
              <Text style={globalStyles.cardDescription}>
                Zero brokerage, 3 month Free PMS, Legal Verification, Dedication
                team and many more.
              </Text>
              <Text style={globalStyles.cardLink}>Browse Properties Now</Text>
            </View>

            <View style={globalStyles.cardDesign}>
              <View style={globalStyles.cardImage}>
                <Image
                  source={{
                    uri: 'https://www.whitegrasscapital.com/assets/img/rental.jpg',
                  }}
                  style={{
                    width: 200,
                    height: 200,
                  }}
                />
              </View>

              <Text style={globalStyles.cardHeader}>Leasing and Rentals</Text>
              <Text style={globalStyles.cardDescription}>
                Corporate or individuals looking for Verified properties,
                dedicated team, post leasing &renting service.
              </Text>
              <Text style={globalStyles.cardLink}>Browse Now</Text>
            </View>

            <View style={globalStyles.cardDesign}>
              <View style={globalStyles.cardImage}>
                <Image
                  source={{
                    uri: 'https://www.whitegrasscapital.com/assets/img/fraction_investment.jpg',
                  }}
                  style={{
                    width: 200,
                    height: 200,
                  }}
                />
              </View>
              <Text style={globalStyles.cardHeader}>Fractional Investment</Text>
              <Text style={globalStyles.cardDescription}>
                Invest in high earning properties, monthly income, EASY Entry &
                Easy EXIT, Investment Appreciation, IRR upto – 18% .
              </Text>
              <Text style={globalStyles.cardLink}>Learn More</Text>
            </View>
          </Animatable.View>
        </View>

        {/* Start About Section */}

        <View style={globalStyles.aboutSection}>
          <ImageBackground
            source={require('/Users/ashish/Projects/whitegrass/WhiteGrass/images/yellow-scratch.png')}
            style={globalStyles.aboutSectionBGImage}>
            <Text style={globalStyles.aboutHeadText}>About Us</Text>
          </ImageBackground>

          <Text style={globalStyles.aboutDetails}>
            At White Grass Capital, we aim to give you a transparent, tech and
            data driven, end to end service and solution for all your Real
            Estate Investment needs so that it gives you can get the highest
            return possible.
          </Text>
          <Text style={globalStyles.aboutSubDetails}>
            Our specialised team focus on how to create maximum value on your
            Real Estate Investments. Our tech driven approach, virtual
            integration and end to end services makes sure you get peace of
            mind. Through our various products, we aim to provide a level
            playing field for all our small and big investors. Through our data
            analyses, regular feedbacks, and ease of use, you can customise and
            diversify your investments as it suits you. We have specialised team
            for NRIs, HNIs &amp; our Corporate customers.
          </Text>
          <TouchableOpacity>
            <Text style={globalStyles.getInTouchButton}>Get In Touch</Text>
          </TouchableOpacity>
        </View>

        {/* End About Section */}

        {/* Start benifit Section */}

        <View style={globalStyles.benifitSection}>
          <View style={globalStyles.benifitSubSection}>
            <Text style={globalStyles.benifitSubSectionHead}>
              Instead of searching various platforms, connect with White Grass
              Capital
            </Text>
            <Text style={globalStyles.benifitSubSectionSubHead}>
              Safe &amp; Reliable, Dedicated team, Complete service.
            </Text>
            <View style={globalStyles.cardDesign}>
              <Text style={globalStyles.benifitCardHeader}>
                Third party Platforms
              </Text>
              <Text style={globalStyles.benifitCardDescription}>
                You connect with multiple individuals
              </Text>
              <Text style={globalStyles.benifitCardListText}>
                <Icon name="close-circle-outline" size={16} />
                Brokerage
              </Text>
              <Text style={globalStyles.benifitCardListTextBlue}>
                <Icon name="checkmark-circle-outline" size={16} />
                Multiple options
              </Text>
              <Text style={globalStyles.benifitCardListText}>
                <Icon name="close-circle-outline" size={16} />
                No end to end Service
              </Text>
              <Text style={globalStyles.benifitCardListText}>
                <Icon name="close-circle-outline" size={16} />
                No Personal assistance
              </Text>
              <Text style={globalStyles.benifitCardListText}>
                <Icon name="close-circle-outline" size={16} />
                No Professional assistance{' '}
              </Text>
              <Text style={globalStyles.benifitCardListText}>
                <Icon name="close-circle-outline" size={16} />
                No after deal closure service
              </Text>
            </View>

            <View style={globalStyles.cardDesign}>
              <Text style={globalStyles.benifitCardHeader}>
                White Grass Platform
              </Text>
              <Text style={globalStyles.benifitCardDescriptionGreen}>
                You connect with one Professional Company
              </Text>
              <Text style={globalStyles.benifitCardListTextBlue}>
                <Icon name="checkmark-circle-outline" size={16} />
                Zero Brokerage
              </Text>
              <Text style={globalStyles.benifitCardListTextBlue}>
                <Icon name="checkmark-circle-outline" size={16} />
                Multiple options
              </Text>
              <Text style={globalStyles.benifitCardListTextBlue}>
                <Icon name="checkmark-circle-outline" size={16} />
                End to end Service
              </Text>
              <Text style={globalStyles.benifitCardListTextBlue}>
                <Icon name="checkmark-circle-outline" size={16} />
                Personal assistance
              </Text>
              <Text style={globalStyles.benifitCardListTextBlue}>
                <Icon name="checkmark-circle-outline" size={16} />
                Professional assistance
              </Text>
              <Text style={globalStyles.benifitCardListTextBlue}>
                <Icon name="checkmark-circle-outline" size={16} />
                After deal closure service
              </Text>
            </View>

            <View style={globalStyles.mobileCardDesign}>
              <Image
                source={{
                  uri: 'https://www.whitegrasscapital.com/assets/img/why us pic.png',
                }}
                style={{width: 280, height: 590}}
              />
            </View>
          </View>
        </View>
        {/* End benifit Section */}

        {/* Start AI2 Section */}

        <View style={globalStyles.ai2Section}>
          <Text style={globalStyles.aboutHead}>
            Algorithm for Investor Intelligence - (AI-2)
          </Text>
          <View style={globalStyles.ai2cardDesign}>
            <Text style={globalStyles.ai2CardList}>
              <Icon
                name="checkmark-circle-outline"
                size={16}
                color={'#3e52ce'}
              />
              Data based leveraging to find out Right opportunity
            </Text>
            <Text style={globalStyles.ai2CardList}>
              <Icon
                name="checkmark-circle-outline"
                size={16}
                color={'#3e52ce'}
              />
              Algorithm-driven insights and quantitative analytics for your
              property portfolio
            </Text>
            <Text style={globalStyles.ai2CardList}>
              <Icon
                name="checkmark-circle-outline"
                size={16}
                color={'#3e52ce'}
              />
              Heatmap Analysis to give understanding of property with higher
              Returns & IRR
            </Text>
          </View>
          <View style={globalStyles.cardDesign}>
            <Image
              source={{
                uri: 'https://www.whitegrasscapital.com/assets/img/ai%202%20algorithm%20pic.png',
              }}
              style={{width: 280, height: 590}}
            />
          </View>
        </View>

        {/* End AI2 Section */}

        {/* Start Web 3.0 Section */}

        <View style={globalStyles.ai2Section}>
          <Text style={globalStyles.aboutHead}>Web 3.0</Text>
          <View style={globalStyles.cardDesign}>
            <Image
              source={{
                uri: 'https://www.whitegrasscapital.com/assets/img/web3.png',
              }}
              style={{width: 280, height: 590}}
            />
          </View>
          <View>
            <Text style={globalStyles.web3Details}>
              White Grass Capital is committed to provide its Commercial Real
              Estate (CRE) Investors, a defining experience though its
              blockchain infrastructure support. It will be leveraged with new
              technology with a single purpose of “Integration”.
            </Text>
            <Text style={globalStyles.web3SubDetails}>Coming 2023</Text>
          </View>
        </View>

        {/* End  Web 3.0  Section */}

        {/* Start Customer Testimonial Section */}

        <View style={globalStyles.customerTestimonial}>
          <Text style={globalStyles.customerTestimonialHead}>
            Customer Testimonial
          </Text>
          <Text style={globalStyles.customerTestimonialSubHead}>
            What our customer have to say about us!
          </Text>
          <View style={globalStyles.customerTestimonialSubSection}>
            <View style={globalStyles.customerTestimonialCarousel}>
              <CarouselCards />
            </View>
          </View>
        </View>

        {/* End  Customer Testimonial Section */}

        {/* Start Latest News Section */}

        <View style={globalStyles.customerTestimonial}>
          <Text style={globalStyles.latestNewsHead}>Latest News</Text>

          <View style={globalStyles.customerTestimonialSubSection}>
            <View style={globalStyles.customerTestimonialCarousel}>
              <CarouselNewsCards />
            </View>
          </View>
        </View>

        {/* End  latest News Section */}

        {/* Start Corporate HNI Section */}

        <View style={globalStyles.ai2Section}>
          <Text style={globalStyles.aboutHead}>
            Are you an Corporate, NRI or HNI?
          </Text>
          <Text style={globalStyles.corporateSubHead}>
            We have got experienced, dedicated & personalized team for all your
            Real Estate Investment or Leasing needs
          </Text>
          <View style={globalStyles.corporateCardDesign}>
            <Text style={globalStyles.corporateCardList}>
              <Icon
                name="checkmark-circle-outline"
                size={16}
                color={'#3e52ce'}
              />
              Complete online assistance
            </Text>
            <Text style={globalStyles.corporateCardList}>
              <Icon
                name="checkmark-circle-outline"
                size={16}
                color={'#3e52ce'}
              />
              Multiple products
            </Text>
            <Text style={globalStyles.corporateCardList}>
              <Icon
                name="checkmark-circle-outline"
                size={16}
                color={'#3e52ce'}
              />
              Post Deal closure service
            </Text>
            <Text style={globalStyles.corporateCardList}>
              <Icon
                name="checkmark-circle-outline"
                size={16}
                color={'#3e52ce'}
              />
              Legal assistance
            </Text>
          </View>
          <View style={globalStyles.cardDesign}>
            <Image
              source={{
                uri: 'https://www.whitegrasscapital.com/assets/img/AreYouCorporate.png',
              }}
              style={{width: 280, height: 590}}
            />
          </View>
        </View>

        {/* End Corporate HNI Section */}

        {/* Start Contact Us Section */}
        {/*
        <View style={globalStyles.ai2Section}>
          <Text style={globalStyles.aboutHead}>Contact Us</Text>
          <View>
            <TextInput
              style={globalStyles.input}
              placeholder="Your Name"
              placeholderTextColor="#000"
              keyboardType="ascii-capable	              "
              onChangeText={text => this.updateValue(text, 'name')}
            />
            <TextInput
              style={globalStyles.input}
              placeholder="Your Email"
              placeholderTextColor="#000"
              keyboardType="email-address"
              onChangeText={text => this.updateValue(text, 'email')}
            />
            <TextInput
              style={globalStyles.input}
              placeholder="Phone Number"
              placeholderTextColor="#000"
              keyboardType="numeric"
              onChangeText={text => this.updateValue(text, 'phonenumber')}
            />
            <View style={globalStyles.Picker}>
              <Picker
                selectedValue={this.state.enquiryType}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({enquiryType: itemValue})
                }>
                <Picker.Item label="Select Type of Enquiry" value="Select" />
                <Picker.Item
                  label="Corporate Enquiry"
                  value="Corporate Enquiry"
                />
                <Picker.Item label="I am investor" value="I am investor" />
                <Picker.Item label="I am buyer" value="I am buyer" />
                <Picker.Item label="I am NRI" value="I am NRI" />
                <Picker.Item
                  label="Business Interest"
                  value="Business Interest"
                />
                <Picker.Item
                  label="Franchise Enquiry"
                  value="Franchise Enquiry"
                />
                <Picker.Item
                  label="Need to list my property"
                  value="Need to list my property"
                />
                <Picker.Item label="I am agent" value="I am agent" />
                <Picker.Item label="General Enquiry" value="General Enquiry" />
                <Picker.Item label="Others" value="Others" />
              </Picker>
            </View>

            <TextInput
              multiline={true}
              numberOfLines={10}
              style={{
                height: 110,
                textAlignVertical: 'top',
                marginBottom: 20,
                paddingLeft: 15,
                fontSize: 16,
                backgroundColor: '#D3D3D3',
                borderBottomColor: 'black',
                borderBottomWidth: 2,
              }}
              placeholder="Your Message"
              placeholderTextColor="#000"
              keyboardType="text"
              onChangeText={text => this.updateValue(text, 'message')}
            />
          </View>

          <TouchableOpacity onPress={() => this.submit()}>
            <Text style={globalStyles.getInTouchButton}>Send Message </Text>
          </TouchableOpacity>
        </View>
            */}
        {/* End Contact Us Section */}
      </ScrollView>
    );
  }
}
