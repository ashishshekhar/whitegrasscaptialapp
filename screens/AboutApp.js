import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Button,
  TouchableOpacity,
  ImageBackground,
  TextInput,
} from 'react-native';

import {globalStyles} from '../styles/global';

export default class AboutApp extends React.Component {
  render() {
    return (
      <ScrollView>
        <View style={globalStyles.aboutSection}>
          <ImageBackground
            source={require('/Users/ashish/Projects/whitegrass/WhiteGrass/images/yellow-scratch.png')}
            style={globalStyles.aboutSectionBGImage}>
            <Text style={globalStyles.aboutHeadText}>About App</Text>
          </ImageBackground>
          <Text style={globalStyles.aboutAppHeader}>
            Metacorp Triangle Assets Pvt Ltd
          </Text>
          <Text style={globalStyles.aboutAppHeaderDetails}>
            At Metacorp Triangle Assets Pvt Ltd, we aim to give you a
            transparent, tech and data driven, end to end service and solution
            for all your Real Estate Investment needs so that it gives you can
            get the highest return possible.
          </Text>
          <Text style={globalStyles.aboutAppHeaderEmail}>
            Email Us : desk@whitegrasscapital.com
          </Text>
          <Text style={globalStyles.aboutAppHeaderEmail}>
            App Version : 0.3
          </Text>
        </View>
      </ScrollView>
    );
  }
}
