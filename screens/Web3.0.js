import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Button,
  TouchableOpacity,
  ImageBackground,
  TextInput,
} from 'react-native';

import {globalStyles} from '../styles/global';

export default class Web3 extends React.Component {
  render() {
    return (
      <ScrollView>
        <View style={globalStyles.ai2Section}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View>
              <Text style={globalStyles.aboutHead}>Web 3.0 </Text>
            </View>
            <View style={{flex: 1, height: 2, backgroundColor: '#ffc451'}} />
          </View>
          <View style={globalStyles.cardDesign}>
            <Image
              source={{
                uri: 'https://www.whitegrasscapital.com/assets/img/web3.png',
              }}
              style={{width: 280, height: 590}}
            />
          </View>
          <View>
            <Text style={globalStyles.web3Details}>
              White Grass Capital is committed to provide its Commercial Real
              Estate (CRE) Investors, a defining experience though its
              blockchain infrastructure support. It will be leveraged with new
              technology with a single purpose of “Integration”.
            </Text>
            <Text style={globalStyles.web3SubDetails}>Coming 2023</Text>
          </View>
        </View>
      </ScrollView>
    );
  }
}
