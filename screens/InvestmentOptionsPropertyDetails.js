import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Button,
  TouchableOpacity,
  ImageBackground,
  TextInput,
} from 'react-native';

import {globalStyles} from '../styles/global';
import Icon from 'react-native-vector-icons/Ionicons';

export default class InvestmentOptionsPropertyDetail extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
    };
  }
  componentDidMount() {
    this.apiCall();
  }
  async apiCall() {
    let resp = await fetch(
      'https://www.whitegrasscapital.com/Api/investment_details/134',
    );
    let respJson = await resp.json();
    this.setState({data: respJson.data});
  }
  goBackToAvailableProperties() {
    this.props.navigation.navigate('Investment Options');
  }

  render() {
    return (
      <ScrollView>
        <Image
          source={{
            uri: 'https://www.whitegrasscapital.com/assets/img/rental.jpg',
          }}
          style={{
            width: 400,
            height: 200,
          }}
        />
        <View style={globalStyles.PropertyDetailLayout}>
          <View style={globalStyles.onlyOnWGCmain}>
            <Text style={globalStyles.onlyOnWGC}>
              &#x2713; Only on WhiteGrassCapital
            </Text>
          </View>

          <View style={globalStyles.DetailPriceRow}>
            {this.state.data.map(obj => (
              <View>
                <View>
                  <Text style={globalStyles.DetailInfoTitle}>{obj.title}</Text>
                </View>
                <View style={globalStyles.DetailInformationSection}>
                  <View>
                    <Text style={globalStyles.DetailInfoType}>Price</Text>
                    <Text style={globalStyles.DetailInfoText}>
                      &#8377; {obj.investments}
                    </Text>
                  </View>
                  <View style={globalStyles.DetailInfoRowRight}>
                    <Text style={globalStyles.DetailInfoType}>Total Area</Text>
                    <Text style={globalStyles.DetailInfoText}>
                      {obj.total_unit} Square Feet
                    </Text>
                  </View>
                </View>

                <View style={globalStyles.DetailInformationSection}>
                  <View>
                    <Text style={globalStyles.DetailInfoType}>Status</Text>
                    <Text style={globalStyles.DetailInfoText}>
                      {obj.status}
                    </Text>
                  </View>
                  <View style={globalStyles.DetailInfoRowRight}>
                    <Text style={globalStyles.DetailInfoType}>
                      Distribution
                    </Text>
                    <Text style={globalStyles.DetailInfoText}>
                      {obj.distributions}
                    </Text>
                  </View>
                </View>

                <View style={globalStyles.DetailInformationSection}>
                  <View>
                    <Text style={globalStyles.DetailInfoType}>Facing</Text>
                    <Text style={globalStyles.DetailInfoText}>East Facing</Text>
                  </View>
                  <View style={globalStyles.DetailInfoRowRight}>
                    <Text style={globalStyles.DetailInfoType}>
                      Furnished St
                    </Text>
                    <Text style={globalStyles.DetailInfoText}>Unfurnished</Text>
                  </View>
                </View>
                <View
                  style={{
                    borderBottomColor: 'gray',
                    borderBottomWidth: StyleSheet.hairlineWidth,
                    paddingTop: 30,
                  }}></View>
                <View style={globalStyles.DetailPropertDetSection}>
                  <Text style={globalStyles.PropertyDetailText}>
                    More Details
                  </Text>
                  <View style={globalStyles.DetailPropertDetRow}>
                    <Text style={globalStyles.DetailInfoType}>
                      Pre Leased :
                    </Text>
                    <Text style={globalStyles.DetailInfoText}>{''} Yes</Text>
                  </View>
                  <View style={globalStyles.DetailPropertDetSection}>
                    <View style={globalStyles.DetailPropertDetRow}>
                      <Text style={globalStyles.DetailInfoType}>Parking :</Text>
                      <Text style={globalStyles.DetailInfoText}>
                        {' '}
                        {obj.parking}
                      </Text>
                    </View>
                  </View>
                  <View style={globalStyles.DetailPropertDetSection}>
                    <View style={globalStyles.DetailPropertDetRow}>
                      <Text style={globalStyles.DetailInfoType}>
                        Posted /Reposted On :
                      </Text>
                      <Text style={globalStyles.DetailInfoText}>
                        {' '}
                        {obj.Posted_on}
                      </Text>
                    </View>
                  </View>
                  <View style={globalStyles.DetailPropertDetSection}>
                    <View style={globalStyles.DetailPropertDetRow}>
                      <Text style={globalStyles.DetailInfoType}>
                        WGC Verified :
                      </Text>
                      <Text style={globalStyles.DetailInfoText}>
                        {' '}
                        {obj.wgc_verfied}
                      </Text>
                    </View>
                  </View>
                  <View style={globalStyles.DetailPropertDetSection}>
                    <View style={globalStyles.DetailPropertDetRow}>
                      <Text style={globalStyles.DetailInfoType}>
                        WGC Ranking :
                      </Text>
                      <Text style={globalStyles.DetailInfoText}>
                        {' '}
                        {obj.wgc_ranking}
                      </Text>
                    </View>
                  </View>

                  <View style={globalStyles.DetailPropertDetSection}>
                    <View style={globalStyles.DetailPropertDetRow}>
                      <Text style={globalStyles.DetailInfoType}>
                        Targeted Investor IRR:
                      </Text>
                    </View>
                    <Text style={globalStyles.DetailInfoRemarks}>
                      {obj.targeted_investor}
                    </Text>
                  </View>
                  <View style={globalStyles.DetailPropertDetSection}>
                    <View style={globalStyles.DetailPropertDetRow}>
                      <Text style={globalStyles.DetailInfoType}>
                        Remarks :-
                      </Text>
                    </View>
                    <Text style={globalStyles.DetailInfoRemarks}>
                      {' '}
                      {obj.remarks}
                    </Text>
                  </View>
                </View>
              </View>
            ))}
          </View>

          <TouchableOpacity onPress={() => this.goBackToAvailableProperties()}>
            <Text style={globalStyles.showPropertiesButton}>
              Go Back To Available Properties
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}
