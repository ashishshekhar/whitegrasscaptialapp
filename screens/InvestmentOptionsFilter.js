import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Button,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  Modal,
  Alert,
  Pressable,
  TouchableHighlight,
} from 'react-native';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';

import {Picker} from '@react-native-picker/picker';
import Slider from '@react-native-community/slider';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import Icon from 'react-native-vector-icons/Ionicons';
import {globalStyles} from '../styles/global';
import {validatePathConfig} from '@react-navigation/native';
import FontAwesome, {
  SolidIcons,
  RegularIcons,
  BrandIcons,
  parseIconFromClassName,
} from 'react-native-fontawesome';
var preLeased = [
  {label: 'Yes', value: 0},
  {label: 'No', value: 1},
];
export default class InvestmentOptionsFilter extends React.Component {
  constructor() {
    super();
    this.state = {
      investmentrange: 350,
      statename: '',
      propertytype: '',
      modalVisible: false,
      communicate: '',
      dataset: [],
      name: '',
      email: '',
      phonenumber: '',
      minBudget: '',
      maxBudget: '',
      areaType: '',
      minArea: '',
      maxArea: '',
      minFloor: '',
      maxFloor: '',
      state: '',
      city: '',
      location: '',
      stateList: [],
      cityList: [],
      locationList: [],
      FlatbackgroundColor: 'white',
      HousebackgroundColor: 'white',
      PlotbackgroundColor: 'white',
      OfficebackgroundColor: 'white',
      ShopbackgroundColor: 'white',
      UCBackgroundColor: 'white',
      RTMBackgroundColor: 'white',
      rentRange: [1000, 500000],
      areaRange: [1, 5000],
      pressed: [],
      status: '',
      type: [],
      preLeased: '',
    };
  }
  multiSliderrentRangeChange = rentRange => {
    this.setState({
      rentRange,
    });
  };
  multiSliderareaRangeChange = areaRange => {
    this.setState({
      areaRange,
    });
  };

  changeFlatColor() {
    this.state.FlatbackgroundColor == '#ffc451'
      ? this.setState({pressed: false, FlatbackgroundColor: 'white'})
      : this.setState({pressed: true, FlatbackgroundColor: '#ffc451'});
  }
  changeOfficeColor() {
    this.state.OfficebackgroundColor == '#ffc451'
      ? this.setState({pressed: false, OfficebackgroundColor: 'white'})
      : this.setState({pressed: true, OfficebackgroundColor: '#ffc451'});
  }
  changeShopColor() {
    this.state.ShopbackgroundColor == '#ffc451'
      ? this.setState({pressed: false, ShopbackgroundColor: 'white'})
      : this.setState({pressed: true, ShopbackgroundColor: '#ffc451'});
  }

  //For status
  changeUCColor() {
    this.state.UCBackgroundColor == '#ffc451'
      ? this.setState({pressed: false, UCBackgroundColor: 'white'})
      : this.setState({
          pressed: true,
          UCBackgroundColor: '#ffc451',
          RTMBackgroundColor: 'white',
        });
  }
  changeRTMColor() {
    this.state.RTMBackgroundColor == '#ffc451'
      ? this.setState({pressed: false, RTMBackgroundColor: 'white'})
      : this.setState({
          pressed: true,
          RTMBackgroundColor: '#ffc451',
          UCBackgroundColor: 'white',
        });
  }

  updateValue(text, field) {
    if (field == 'name') {
      this.setState({
        name: text,
      });
    } else if (field == 'email') {
      this.setState({
        email: text,
      });
    } else if (field == 'phonenumber') {
      this.setState({
        phonenumber: text,
      });
    } else if (field == 'message') {
      this.setState({
        message: text,
      });
    }
  }
  submit() {
    let collection = {};
    (collection.name = this.state.name),
      (collection.email = this.state.email),
      (collection.phonenumber = this.state.phonenumber),
      (collection.enquirytype = this.state.communicate);

    var url = 'https://www.whitegrasscapital.com/api/contactmail';

    fetch(url, {
      method: 'POST', // or 'PUT'
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(collection),
    })
      .then(response => response.json())
      .then(response => {
        console.log('Success:', response);
      })
      .catch(error => {
        console.error('Error:', error);
      });
    Alert.alert('Success!', 'We will get back to you.');
  }
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  showProperties() {
    var state = this.state.state;
    var city = this.state.city;
    var location = this.state.location;
    var rentRange = this.state.rentRange;
    var areaRange = this.state.areaRange;
    var preLeased = this.state.preLeased;
    var filtertype = [];
    // Commercial
    if (this.state.FlatbackgroundColor == '#ffc451') {
      filtertype.push('13');
    }
    //Office Space
    if (this.state.OfficebackgroundColor == '#ffc451') {
      filtertype.push('6');
    }
    //Show room
    if (this.state.ShopbackgroundColor == '#ffc451') {
      filtertype.push('15');
    }
    if (this.state.UCBackgroundColor == '#ffc451') {
      var ways = 0;
    }
    if (this.state.RTMBackgroundColor == '#ffc451') {
      var ways = 1;
    }
    console.log(filtertype);
    //this.props.navigation.navigate('Commercial Leasing');
    this.props.navigation.navigate('Investment Options', {
      state,
      location,
      city,
      rentRange,
      areaRange,
      filtertype,
      ways,
      preLeased,
    });
  }

  componentDidMount() {
    return fetch('https://www.whitegrasscapital.com/Api/get_state', {
      method: 'POST',
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          stateList: responseJson.data.states,
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  onSateValueChange = (itemValue, itemPosition) => {
    if (itemValue == '') {
      Alert.alert('Please select state.');
    } else {
      this.setState({state: itemValue});
      return fetch(
        'https://www.whitegrasscapital.com/Api/get_cities?state_id=' +
          itemValue,
        {
          method: 'POST',
        },
      )
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            cityList: responseJson.data.city,
          });
        })
        .catch(error => {
          console.error(error);
        });
    }
  };
  onCityValueChange = (itemValue, itemPosition) => {
    if (itemValue == '') {
      Alert.alert('Please select city.');
    } else {
      this.setState({city: itemValue});
      return fetch(
        'https://www.whitegrasscapital.com/Api/get_location?city_id=' +
          itemValue,
        {
          method: 'POST',
        },
      )
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            locationList: responseJson.data.locationdata,
          });
        })
        .catch(error => {
          console.error(error);
        });
    }
  };

  render() {
    return (
      <ScrollView>
        {/* New Filter Design Starts */}

        {/*Adding this to check commit and push*/}
        <View style={globalStyles.NewFilterLayout}>
          <View style={globalStyles.NewFilterLayout}>
            <Text style={globalStyles.FilterLabels}>State</Text>
            <View style={globalStyles.BudgetDropdownLayout}>
              <View style={globalStyles.AreaDropdown}>
                <Picker
                  selectedValue={this.state.state}
                  onValueChange={(itemValue, itemIndex) => {
                    this.onSateValueChange(itemValue);
                  }}>
                  <Picker.Item label="State" value="" />
                  {this.state.stateList.map(obj => (
                    <Picker.Item label={obj.name} value={obj.id} />
                  ))}
                </Picker>
              </View>
            </View>
            <Text style={globalStyles.FilterLabels}>City</Text>
            <View style={globalStyles.AreaDropdown}>
              <Picker
                selectedValue={this.state.city}
                onValueChange={(itemValue, itemIndex) => {
                  this.onCityValueChange(itemValue);
                }}>
                <Picker.Item label="City" value="" />
                {this.state.cityList.map(obj => (
                  <Picker.Item label={obj.name} value={obj.id} />
                ))}
              </Picker>
            </View>
            <Text style={globalStyles.FilterLabels}>Location</Text>
            <View style={globalStyles.AreaDropdown}>
              <Picker
                selectedValue={this.state.location}
                onValueChange={(itemValue, itemIndex) => {
                  if (itemValue == '') {
                    Alert.alert('Please select location.');
                  } else {
                    this.setState({location: itemValue});
                  }
                }}>
                <Picker.Item label="Location" value="" />
                {this.state.locationList.map(obj => (
                  <Picker.Item label={obj.name} value={obj.id} />
                ))}
              </Picker>
            </View>
          </View>

          <View style={globalStyles.NewFilterLayout}>
            <Text style={globalStyles.FilterLabels}>Area (Sq.Ft)</Text>
            <View style={{marginLeft: 25}}>
              <MultiSlider
                style={{width: 390, marginLeft: 15}}
                values={[this.state.areaRange[0], this.state.areaRange[1]]}
                sliderLength={275}
                onValuesChange={this.multiSliderareaRangeChange}
                min={1}
                max={5000}
                step={1}
                thumbTintColor="rgb(252, 228, 149)"
                thumbTintSize="100"
              />
            </View>
            <View style={globalStyles.MinMaxContainer}>
              <View style={globalStyles.item}>
                <Text style={globalStyles.MinMaxFilterLabels}>
                  Min : {this.state.areaRange[0]}
                </Text>
              </View>
              <View style={globalStyles.item}>
                <Text style={globalStyles.MinMaxFilterLabels}>
                  Max : {this.state.areaRange[1]}
                </Text>
              </View>
            </View>
          </View>

          <View style={globalStyles.NewFilterLayout}>
            <Text style={globalStyles.FilterLabels}>Rent( Rs. )</Text>
            <View style={{marginLeft: 25}}>
              <MultiSlider
                style={{width: 350, marginLeft: 15}}
                values={[this.state.rentRange[0], this.state.rentRange[1]]}
                sliderLength={275}
                onValuesChange={this.multiSliderrentRangeChange}
                min={1000}
                max={500000}
                step={5000}
                thumbTintColor="rgb(252, 228, 149)"
                thumbTintSize="30"
              />
            </View>
            <View style={globalStyles.MinMaxContainer}>
              <View style={globalStyles.item}>
                <Text style={globalStyles.MinMaxFilterLabels}>
                  Min : {this.state.rentRange[0]}
                </Text>
              </View>
              <View style={globalStyles.item}>
                <Text style={globalStyles.MinMaxFilterLabels}>
                  Max : {this.state.rentRange[1]}
                </Text>
              </View>
            </View>
          </View>
          <View style={globalStyles.NewFilterLayout}>
            <Text style={globalStyles.FilterLabels}>Status</Text>
            <View style={globalStyles.BudgetDropdownLayout}>
              <TouchableOpacity
                style={{
                  backgroundColor: this.state.UCBackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeUCColor()}
                values={[this.state.pressed]}>
                <View>
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    Under Construction
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: this.state.RTMBackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeRTMColor()}>
                <View>
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    Ready to Move
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <View style={globalStyles.NewFilterLayout}>
            <Text style={globalStyles.FilterLabels}>Type</Text>
            <ScrollView
              style={globalStyles.PropertyTypeIconLayout}
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              <TouchableOpacity
                style={{
                  backgroundColor: this.state.OfficebackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeOfficeColor()}>
                <View>
                  <FontAwesome
                    style={globalStyles.iconStyle}
                    icon={SolidIcons.laptop}
                  />
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    Land
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={{
                  backgroundColor: this.state.HousebackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeHouseColor()}>
                <View>
                  <FontAwesome
                    style={globalStyles.iconStyle}
                    icon={SolidIcons.building}
                  />
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    Warehouse
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={{
                  backgroundColor: this.state.ShopbackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeShopColor()}>
                <View>
                  <FontAwesome
                    style={globalStyles.iconStyle}
                    icon={SolidIcons.shoppingBasket}
                  />
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    Show Room
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={{
                  backgroundColor: this.state.OfficebackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 100,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeOfficeColor()}>
                <View>
                  <FontAwesome
                    style={globalStyles.iconStyle}
                    icon={SolidIcons.laptop}
                  />
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    Office
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={{
                  backgroundColor: this.state.FlatbackgroundColor,
                  borderWidth: 1,
                  borderColor: '#ebebeb',
                  borderRadius: 5,
                  width: 150,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginLeft: 15,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                onPress={() => this.changeFlatColor()}>
                <View>
                  <FontAwesome
                    style={globalStyles.iconStyle}
                    icon={SolidIcons.building}
                  />
                  <Text style={globalStyles.NewFilterPropertyTypeIconText}>
                    Commercial Space
                  </Text>
                </View>
              </TouchableOpacity>
            </ScrollView>
          </View>
          <View style={globalStyles.NewFilterLayout}>
            <Text style={globalStyles.FilterLabels}>Pre Leased</Text>
            <View style={globalStyles.ReraRadioForm}>
              <RadioForm
                radio_props={preLeased}
                onPress={value => {
                  0;
                }}
                initial={-1}
                buttonSize={10}
                selectedButtonColor={'#ffc451'}
                buttonColor={'#ffc451'}
                labelStyle={{fontSize: 16, color: 'black'}}
              />
            </View>
          </View>
          <TouchableOpacity onPress={() => this.showProperties()}>
            <Text style={globalStyles.showPropertiesButton}>
              Show Properties
            </Text>
          </TouchableOpacity>
        </View>
        {/* New Filter Design Ends */}
      </ScrollView>
    );
  }
}