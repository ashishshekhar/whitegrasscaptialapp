import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Button,
  TouchableOpacity,
  ImageBackground,
  TextInput,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';

import {globalStyles} from '../styles/global';

export default class InvestorLogin extends React.Component {
  render() {
    return (
      <ScrollView>
        {/* Start Contact Us Section */}

        <View style={globalStyles.ai2Section}>
          <Text style={globalStyles.loginHeader}>Login</Text>
          <Text style={globalStyles.loginSubHeader}>
            Please enter your login info to continue
          </Text>
          <View>
            <TextInput
              style={globalStyles.input}
              placeholder="Email Address"
              keyboardType="text"
            />
            <TextInput
              style={globalStyles.input}
              placeholder="Password"
              keyboardType="text"
            />

            <View style={{flexDirection: 'row'}}>
              <CheckBox value={true} />
              <Text style={globalStyles.remember}>Remember Me</Text>
              <TouchableOpacity style={globalStyles.fogotTextSec}>
                <Text style={globalStyles.fogotText}>Forgot Password ?</Text>
              </TouchableOpacity>
            </View>
          </View>

          <TouchableOpacity style={globalStyles.loginButtonSec}>
            <Text style={globalStyles.loginButton}>Login</Text>
          </TouchableOpacity>
        </View>

        {/* End Contact Us Section */}
      </ScrollView>
    );
  }
}
