import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Button,
  TouchableOpacity,
  ImageBackground,
  TextInput,
} from 'react-native';
import Accordian from '../Accordian';
import {globalStyles} from '../styles/global';

export default class FractionInvestmentFAQ extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menu: [
        {
          title: 'Q. What is Fractional Investments ?',
          data: `White Grass Capital is a tech enabled commercial real estate investment platform that allows its users to access high return properties with minimum investments through Fractional Investment options. This properties may be Retail space, Warehouse, Holiday properties or any other commercial spaces. 
  
For each property, a a Special Purpose Vehicle (SPV) is created in which funds are raised to purchase, own and manage the property. 

A Special Purpose Vehicle is an entity incorporated/created under the law, being a Partnership firm, LLP, a private company, etc., for a specific lawful purpose.

The Fractional Investment opportunity listed on the White Grass Capital platform will be owned by an SPV being a private limited company set up for this specific purpose. 

Your investment shall be towards subscription of the shares of the SPV that holds the property and will represents your fractional investment.`,
        },
        {
          title:
            'Q. How Fractional Investment Works complete process happens ?',
          data: `
1. Search and finds the properties which best suits your pocket size and return expectations.
          
2. Choose options how you want we connect with you.

3. Once we mutually curate and finalise the best deal for you, Invest.

4. Start receiving cash distributions on your investment straight to your bank account monthly.

5. Through our AI2
Use our Portal to track your Investment, Returns and expected future performance which is powered by Data and Tech driven Algorithm for Investor Intelligence (AI2).

6. EXIT easy with your shares just, without thinking and headache of the complete property.
`,
        },

        {
          title: 'Q.  What are its advantages ?',
          data: `
1. Low investment entry

2. Easy EXIT 

3. Portfolio diversification

4. Monthly Cash Flow

5. Dedicated legal & property management team`,
        },
        {
          title: 'Q. What are the the available options ?',
          data: 'For Properties available to invest with fractional investment option, kindly visit ( https://www.whitegrasscapital.com/Investment ) and on top “FILTER” option, select “YES” on “Fractional Investment” filter.',
        },
        {
          title: 'Q. What are the legal verification of the properties ?',
          data: 'Before availing any Property for “Fractional Investment” to our investors on White Grass Capital, our legal team check, access and do a thorough legal verification of the property.',
        },
        {
          title:
            'Q. Who manage the property income generation and monthly cash flow part ?',
          data: `White Grass Capital will be responsible to provide complete asset and property management services to the SPV and undertake accounting, secretarial, reporting, leasing, maintenance, and other operational aspects under the asset management services contract with the SPV. Hence a complete holistic and professional service is been provided to our investors.`,
        },
        {
          title: 'Q. How are the income distributed ? Is it Yearly or Monthly?',
          data: 'Monthly, mostly by the 7 th of every next month.',
        },
        {
          title: 'Q. Does any fix return is guaranteed ?',
          data: 'No. Only after assessing the Property, that it can yield a good return to our investors, we put it on offer, but we give you only Projections (on every our presentations, marketing materials or discussions) based on our estimates.',
        },
        {
          title: 'Q. How can I EXIT ?',
          data: 'Every 5 year, or earlier, once we realise, that the Property have shown good progress in terms of multiplying its Market Value, we give our investors a EXIT option.',
        },
        {
          title: 'Q. How can I EXIT early if suddenly my need comes ?',
          data: 'For Properties available to invest with fractional investment option, kindly visit ( https://www.whitegrasscapital.com/Investment ) and on top “FILTER” option, select “YES” on “Fractional Investment” filter.',
        },
        {
          title:
            'Q. Is it possible to get all paper and legal work related to my Investments done digitally?',
          data: 'Yes. We encourage digital process at the comfort of your home or office or any your location. All required documentation can be signed and accepted digitally through our reputed digital signature provider.',
        },
        {
          title: 'Q. How WGC Earns through my Investment ?',
          data: `
1. A total of 5% Carry Charges and Management fee on your Investments.

2. We charge 5% - 20% Maintenance charges on the monthly income pay-out for our “Property Maintenance & Services”. This charges only happens on the net income each property earned. Charges vary depending on the type of Property, since every property require different level of maintenance cost. This charges is mentioned in advance to you, on every property profile and marketing materials.
 
3. During EXIT, a performance fee of 20% on the gains above a hurdle rate of 10% IRR will be charged.

Lets understand this by example- let us take a scenario where a sale of property happens after 5 years. On a property value of 25,00,000 lakh, gains calculated at 10% IRR after 5 years would be ~Rs. 13,27,500.(Sale Value + Rentals-Cost of Investment)

If the investor realises gains on the property of Rs. 14,00,000 (Sale Value + Rentals - Cost of Investment) we would charge 20% on Rs. 72,500 (14,00,000-13,27,500) i.e. Rs 14,500.
`,
        },
        {
          title:
            'Q. How to know which real estate deal will give me high return ?',
          data: 'For Properties available to invest with fractional investment option, kindly visit ( https://www.whitegrasscapital.com/Investment ) and on top “FILTER” option, select “YES” on “Fractional Investment” filter. Then decide which investments suits you the most and proceed.          ',
        },
        {
          title: 'Q. How safe are my investments ?',
          data: 'All Investments are subject to market risk. However, historically, Real Estate do give a good return if invested with the long term horizon.          ',
        },
        {
          title: 'Q. How can I track my investments?',
          data: 'Once investment process completes, we give you the credentials (Investor ID & Password) where you can track and manage your Investments. Also our Portal gives you the added advantage to track your Investment, Returns and expected future performance which is powered by Data and Tech driven “Algorithm for Investor Intelligence (AI2)”.',
        },
        {
          title: 'Q. What are the taxes implications ?',
          data: `
For Indian residents, you will be paying taxes on rental payouts and on capital appreciation.

Rents : Rents received from the property are distributed as interest on debentures. It is taxable in the hands of the investors under “Income from Other Sources” at the applicable tax slab.

Capital Appreciation : Capital appreciation is subject to capital gain tax at applicable rates. The applicable tax rate would depend on the period for which the shares and debentures were held (short-term vs long- term).

Short-term Capital Gain will be applicable if the shares and debentures are sold before 24 and 36 months respectively This will be taxed at the rate applicable to the investor.
          
Long-term capital Gain will be applicable if the Shares & Debentures are held for more than 24 and 36 months respectively. It will be taxed at 20%, irrespective of the quantum of gains.
          
The benefit of indexation may be explored in the case of long-term capital gains. (Holding Period > 2 years for shares and 3 years for Debentures).
          `,
        },
        {
          title: 'Q. What are the taxes implications if I am NRI ?',
          data: `
Under Indian income-tax law, an NRI is required to pay tax on any income earned or sourced in India. If the income in India exceeds the basic exemption limit, the NRI will have to pay taxes in India as per the applicable slab rates.

Rent Income : It will be distributed as interest on debentures. It will be taxed at the applicable tax rate or the rates of the tax treaty, whichever is beneficial to the investor.

Appreciation: Capital appreciation is subject to capital gain tax at applicable rates. The applicable tax rate would depend on the period for which the asset was held (short-term vs long-term).

Short-term Capital Gain will be applicable if the shares and debentures are sold before 24 and 36 months respectively This will be taxed at the rate applicable to the investor.

Short Term Capital Gain will be applicable if the Shares & Debentures are sold before 24 months and 36 months respectively. It will be taxable as short-term capital gains at applicable tax rates for the respective NRI(s).

Long-term capital Gain will be applicable if the Shares & Debentures are held for more than 24 months and 36 months respectively. It will be taxed at 10%, irrespective of the quantum of gains.

NRIs can explore benefits under the Double Taxation Avoidance Agreement (“DTAA”) entered with the respective country, subject to the availability of a Tax residency Certificate.

          `,
        },
        {
          title: 'Q. What are the tax deductions on the rental income ?',
          data: `
Yes. The SPV deducts a 10% TDS before remitting returns to Resident Indians and 20.8% for NRI Investors.

Resident Indians can submit Form 15G/15H and NRI’s* can submit TRC for reduced TDS.

NRIs can explore benefits under the Double Taxation Avoidance Agreement (“DTAA”) entered with the respective country, subject to the availability of a Tax Residency Certificate (“TRC”).
          `,
        },
        {
          title:
            'Q. What are the investment stages and documentation process ?',
          data: `
At first, You will need to submit the following document :-

1. A copy of your PAN Card.

2. Address Proof such as Aadhar, Driver's Licence, or Passport.

3. Bank statement or a cancelled cheque leaf with the name printed.

NRI investors need to provide an NRE or NRO account number.

Non-Individual entities will be required to submit additional documents.

Once verified, You will receive all necessary property and SPV-related documents for your perusal and scrutiny.

Once you ok, you can start your Investments under your Account for any listed property on White Grass Capital portal.

You will get all necessary documents including but not limited to Investment receipt, Expression of Interest (EOI, Shareholder’s Agreement of SPV, Equity shares.




          `,
        },
      ],
    };
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        {this.renderAccordians()}
      </ScrollView>
    );
  }

  renderAccordians = () => {
    const items = [];
    for (item of this.state.menu) {
      items.push(<Accordian title={item.title} data={item.data} />);
    }
    return items;
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    fontFamily: 'OpenSans-Regular',
  },
});
