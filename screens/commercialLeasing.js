import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Button,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  Modal,
  Alert,
  Linking,
} from 'react-native';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import {Picker} from '@react-native-picker/picker';
import Slider from '@react-native-community/slider';
import Icon from 'react-native-vector-icons/Ionicons';
import {globalStyles} from '../styles/global';
import {validatePathConfig} from '@react-navigation/native';
import FontAwesome, {
  SolidIcons,
  RegularIcons,
  BrandIcons,
  parseIconFromClassName,
} from 'react-native-fontawesome';

var fractionInvestmentRadio = [
  {label: 'Yes', value: 0},
  {label: 'No', value: 1},
];
var reraRegisterd = [
  {label: 'Rera Registered Properties', value: 0},
  {label: 'Rera Registered Agents', value: 1},
];

var verifiedProperty = [{label: 'Show only verified property', value: 0}];
var showOnly = [
  {label: 'Properties with discounts and offers', value: 0},
  {label: 'Exclusive Properties', value: 1},
  {label: 'Properties posted by certified agents', value: 1},
];
export default class CommercialLeasing extends React.Component {
  constructor() {
    super();
    this.state = {
      investmentrange: 350,
      statename: '',
      propertytype: '',
      modalVisible: false,
      communicate: '',
      name: '',
      email: '',
      phonenumber: '',
      minBudget: '',
      maxBudget: '',
      areaType: '',
      minArea: '',
      maxArea: '',
      minFloor: '',
      maxFloor: '',
      data: [],
    };
  }
  updateValue(text, field) {
    if (field == 'name') {
      this.setState({
        name: text,
      });
    } else if (field == 'email') {
      this.setState({
        email: text,
      });
    } else if (field == 'phonenumber') {
      this.setState({
        phonenumber: text,
      });
    } else if (field == 'message') {
      this.setState({
        message: text,
      });
    }
  }
  submit() {
    let collection = {};
    (collection.name = this.state.name),
      (collection.email = this.state.email),
      (collection.phonenumber = this.state.phonenumber),
      (collection.enquirytype = this.state.communicate);

    var url = 'https://www.whitegrasscapital.com/api/contactmail';

    fetch(url, {
      method: 'POST', // or 'PUT'
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(collection),
    })
      .then(response => response.json())
      .then(response => {
        console.log('Success:', response);
      })
      .catch(error => {
        console.error('Error:', error);
      });
    Alert.alert('Success!', 'We will get back to you.');
  }
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  showFilter() {
    this.props.navigation.navigate('Commercial Leasing Filter');
  }
  showPropertyDetail() {
    this.props.navigation.navigate('Commercial Leasing Property Detail');
  }

  componentDidMount() {
    return fetch('https://www.whitegrasscapital.com/Api/commercial_leasing', {
      method: 'POST',
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          data: responseJson.data.invest_lists,
        });
      })
      .catch(error => {
        console.error(error);
      });
  }
  UNSAFE_componentWillMount() {
    this._subscribe = this.props.navigation.addListener('focus', () => {
      var filterDataSet = this.props.route.params;
     
      if (filterDataSet !== undefined) {
        return fetch(
          'https://www.whitegrasscapital.com/Api/commercial_leasing?state=' +
            filterDataSet.state +
            '&city=' +
            filterDataSet.city +
            '&location=' +
            filterDataSet.location +
            '&square_feet_min=' +
            filterDataSet.areaRange[0] +
            '&square_feet_max=' +
            filterDataSet.areaRange[1] +
            '&rent_min=' +
            filterDataSet.rentRange[0] +
            '&rent_max=' +
            filterDataSet.rentRange[1] +
            '&type=' +
            filterDataSet.filtertype +
            '&ways=' +
            filterDataSet.ways +
            '&' +
            {
              method: 'POST',
            },
        )
          .then(response => response.json())
          .then(responseJson => {
            this.setState({
              data: responseJson.data.invest_lists,
            });
          })
          .catch(error => {
            console.error(error);
          });
      }
    });
  }

  render() {
    return (
      <ScrollView>
        {/* New Filter Design Starts */}

        <View style={globalStyles.NewFilterLayout}>
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              style={globalStyles.FilterTouch}
              onPress={() => this.showFilter()}>
              <Text style={globalStyles.FilterText}>Filters</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={globalStyles.FilterTouch}
              onPress={() => this.componentDidMount()}>
              <Text style={globalStyles.FilterText}>Reset</Text>
            </TouchableOpacity>
          </View>

          {/* New Listing Design Starts */}

          <Text style={globalStyles.PropertyListingHeader}>
            Available Properties :-
          </Text>

          {this.state.data.length > 0 ? (
            <TouchableOpacity onPress={() => this.showPropertyDetail()}>
              {this.state.data.map(obj => (
                <View>
                  <View style={globalStyles.PropertyListingLayout}>
                    <View style={globalStyles.onlyOnWGCmainListing}>
                      <Text style={globalStyles.onlyOnWGC}>
                        &#x2713; Only on WhiteGrassCapital
                      </Text>
                    </View>
                    <Text style={globalStyles.DetailInfoTitle}>
                      {obj.title}
                    </Text>
                    <View style={globalStyles.PropertyListing}>
                      <View>
                        <Image
                          source={{uri: `${obj.banner}`}}
                          style={{
                            width: 150,
                            height: 180,
                          }}
                        />
                        <Text style={globalStyles.propertyFacing}>
                          &#x2714; East Facing Property
                        </Text>
                      </View>
                      <View style={globalStyles.ListingCardRight}>
                        <Text style={globalStyles.ListingCardPrice}>
                          &#8377; {obj.price}
                        </Text>
                        <Text style={globalStyles.ListingCardPrice}>
                          Area : {obj.total_unit} Sq Ft
                        </Text>
                        <Text style={globalStyles.ListingCardPrice}>
                          Status : Ready to Move
                        </Text>
                      </View>
                    </View>
                    <Text style={globalStyles.propertyFacing}>
                      {obj.description}
                    </Text>
                    <View style={globalStyles.CardPhoneSection}></View>
                    <TouchableOpacity
                      onPress={() => {
                        this.setModalVisible(true);
                      }}>
                      <Text style={globalStyles.getStartedButton}>
                        Get Started
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              ))}
            </TouchableOpacity>
          ) : (
            <View>
              <View style={globalStyles.PropertyListingLayout}>
                <Text style={globalStyles.DetailInfoTitle}>
                  No Data Available!
                </Text>
              </View>
            </View>
          )}

          {/* New Listing Design Ends */}
        </View>
        {/* New Filter Design Ends */}
      </ScrollView>
    );
  }
}