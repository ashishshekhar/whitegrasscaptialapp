import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Button,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  Linking,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Accordian from '../Accordian';

import {globalStyles} from '../styles/global';

export default class FullBuyFAQ extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menu: [
        {
          title:
            'Q. What if I visit the property but don’t wish to buy or invest ?',
          data: `We don’t have any pre-requisite or reservations. Till you gets what you wish, you don’t have any compulsion to buy or invest with us.`,
        },
        {
          title: 'Q. What are post deal services you provide ?',
          data: `If you buy or invest with us, as complimentary service, for first 3 month, we dont charge any commission on this our PMS (Property Management & Servives). So lets for example, After buying or investing, if you decide now to unlock the value of your Asset by deciding to putting it up for rent, corporate or other lease, renovation or any such, we provide assistance in all such services.`,
        },
        {
          title: 'Q. Who provide all legal documentation services ?',
          data: `You can engage your lawyer. If you wish, we have dedicated and experienced legal team to do the same too. It will be a payable service.`,
        },
      ],
    };
  }
  render() {
    return (
      <ScrollView>
        <View style={globalStyles.fullBuyContainer}>
          <Text style={globalStyles.advText}>
            Advantage of Buying Through WGC
          </Text>
          <View style={globalStyles.fullBuycardDesign}>
            <View style={globalStyles.iconSec}>
              <Icon
                name="checkbox"
                style={globalStyles.icon}
                size={32}
                color={'black'}></Icon>
            </View>
            <Text style={globalStyles.fullBuycardHeader}>Zero Brokerage</Text>
            <Text style={globalStyles.fullBuycardDescription}>
              We don’t take any brokerage or commission from our Buyer!
            </Text>
          </View>

          <View style={globalStyles.fullBuycardDesign}>
            <View style={globalStyles.iconSec}>
              <Icon
                name="cash"
                style={globalStyles.icon}
                size={32}
                color={'black'}></Icon>
            </View>

            <Text style={globalStyles.fullBuycardHeader}>Complete Service</Text>
            <Text style={globalStyles.fullBuycardDescription}>
              From finding the right deal, to legal assistance to post deal
              service, we have specialized team for all.
            </Text>
          </View>

          <View style={globalStyles.fullBuycardDesign}>
            <View style={globalStyles.iconSec}>
              <Icon
                name="thumbs-up"
                style={globalStyles.icon}
                size={32}
                color={'black'}></Icon>
            </View>
            <Text style={globalStyles.fullBuycardHeader}>Safe & Stability</Text>
            <Text style={globalStyles.fullBuycardDescription}>
              We do Stage 1 verification before posting any deal and provide
              Stage 2 verification when deal is selected by you.
            </Text>
          </View>
          <View style={globalStyles.fullBuycardDesign}>
            <View style={globalStyles.iconSec}>
              <Icon
                name="globe"
                style={globalStyles.icon}
                size={32}
                color={'black'}></Icon>
            </View>
            <Text style={globalStyles.fullBuycardHeader}>Leading Firm</Text>
            <Text style={globalStyles.fullBuycardDescription}>
              We are East India's, one of the Leading Management company
              specialized for Commercial Real Estate Properties!
            </Text>
          </View>
          <View style={globalStyles.fullBuycardDesign}>
            <View style={globalStyles.iconSec}>
              <Icon
                name="stats-chart"
                style={globalStyles.icon}
                size={32}
                color={'black'}></Icon>
            </View>

            <Text style={globalStyles.fullBuycardHeader}>
              Tech based approach
            </Text>
            <Text style={globalStyles.fullBuycardDescription}>
              Inputs provided by you are taken into our Algorithm for Investor
              Intelligence (AI2) and based on historical data , KPIs and
              analytics, we gives you the best personalised investment advice.
            </Text>
          </View>
          <View style={globalStyles.fullBuycardDesign}>
            <View style={globalStyles.iconSec}>
              <Icon
                name="settings"
                style={globalStyles.icon}
                size={32}
                color={'black'}></Icon>
            </View>
            <Text style={globalStyles.fullBuycardHeader}>We Care</Text>
            <Text style={globalStyles.fullBuycardDescription}>
              Our Certified Business Associates (CBA) assist you throughout the
              deal and our dedicated customer care team will be available if any
              need arises.
            </Text>
          </View>
          <View>
            <Text style={globalStyles.advText}>How it works?</Text>
            <Text style={globalStyles.howItWorks}>COMPLETE PROCESS :-</Text>
            <View>
              <View style={globalStyles.fullBuycardDesign}>
                <Text style={globalStyles.stepDesign}>Browse</Text>
                <Text style={globalStyles.fullBuycardDescription}>
                  Browse the deal by clicking -
                </Text>
                <Text
                  style={{color: '#ffc451', fontWeight: 'bold'}}
                  onPress={() => Linking.openURL('http://google.com')}>
                  https://www.whitegrasscapital.com/Investment-Options{' '}
                </Text>
              </View>
              <View style={globalStyles.iconSecWorks}>
                <Icon
                  name="arrow-down"
                  style={globalStyles.icon}
                  size={32}
                  color={'black'}></Icon>
              </View>

              <View style={globalStyles.fullBuycardDesign}>
                <Text style={globalStyles.stepDesign}>Connect with us</Text>
                <Text style={globalStyles.fullBuycardDescription}>
                  Choose options how you want we connect with you.
                </Text>
              </View>

              <View style={globalStyles.iconSecWorks}>
                <Icon
                  name="arrow-down"
                  style={globalStyles.icon}
                  size={32}
                  color={'black'}></Icon>
              </View>

              <View style={globalStyles.fullBuycardDesign}>
                <Text style={globalStyles.stepDesign}>Complete Assistance</Text>
                <Text style={globalStyles.fullBuycardDescription}>
                  From our expert team to our leading Tech powered by Data and
                  Tech driven Algorithm for Investor Intelligence (AI2), they
                  will assist you in choosing the right deal suited to your
                  needs.{' '}
                </Text>
              </View>
              <View style={globalStyles.iconSecWorks}>
                <Icon
                  name="arrow-down"
                  style={globalStyles.icon}
                  size={32}
                  color={'black'}></Icon>
              </View>

              <View style={globalStyles.fullBuycardDesign}>
                <Text style={globalStyles.stepDesign}>
                  Physical visit if need
                </Text>
                <Text style={globalStyles.fullBuycardDescription}>
                  You can then opt to visit the Property to visit physically.
                  Our CBA will assist you on the same.
                </Text>
              </View>
              <View style={globalStyles.iconSecWorks}>
                <Icon
                  name="arrow-down"
                  style={globalStyles.icon}
                  size={32}
                  color={'black'}></Icon>
              </View>

              <View style={globalStyles.fullBuycardDesign}>
                <Text style={globalStyles.stepDesign}>
                  Stage-2 complete legal verification{' '}
                </Text>
                <Text style={globalStyles.fullBuycardDescription}>
                  Once you selected, then you can or we can do the complete
                  legal verification of the property on your behalf.
                </Text>
              </View>
              <View style={globalStyles.iconSecWorks}>
                <Icon
                  name="arrow-down"
                  style={globalStyles.icon}
                  size={32}
                  color={'black'}></Icon>
              </View>

              <View style={globalStyles.fullBuycardDesign}>
                <Text style={globalStyles.stepDesign}>Advance </Text>
                <Text style={globalStyles.fullBuycardDescription}>
                  You can pay 10% and go ahead with the "Buyer's Agreement
                  documentation".
                </Text>
              </View>
              <View style={globalStyles.iconSecWorks}>
                <Icon
                  name="arrow-down"
                  style={globalStyles.icon}
                  size={32}
                  color={'black'}></Icon>
              </View>

              <View style={globalStyles.fullBuycardDesign}>
                <Text style={globalStyles.stepDesign}>
                  Final documentation, Financing & Deal closure
                </Text>
                <Text style={globalStyles.fullBuycardDescription}>
                  Last stage will involve all final documentation, financing (if
                  needed, our team can Assist), registration and final payment.
                </Text>
              </View>
              <View style={globalStyles.iconSecWorks}>
                <Icon
                  name="arrow-down"
                  style={globalStyles.icon}
                  size={32}
                  color={'black'}></Icon>
              </View>

              <View style={globalStyles.fullBuycardDesign}>
                <Text style={globalStyles.stepDesign}>
                  Post Deal Closure Service
                </Text>
                <Text style={globalStyles.fullBuycardDescription}>
                  Once deal close, we provide 3 month Free PMS service and any
                  other assistance if required.
                </Text>
              </View>
            </View>
          </View>
          <View style={globalStyles.faqSec}>
            <Text style={globalStyles.advText}>FAQ </Text>
            <View>{this.renderAccordians()}</View>
          </View>
        </View>
      </ScrollView>
    );
  }
  renderAccordians = () => {
    const items = [];
    for (item of this.state.menu) {
      items.push(<Accordian title={item.title} data={item.data} />);
    }
    return items;
  };
}
