import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Button,
  TouchableOpacity,
  ImageBackground,
  TextInput,
} from 'react-native';
import {Picker} from '@react-native-picker/picker';

import {globalStyles} from '../styles/global';

export default class Contact extends React.Component {
  constructor() {
    super();
    this.state = {
      name: '',
      email: '',
      phonenumber: '',
      message: '',
      enquirytype: '',
    };
  }
  updateValue(text, field) {
    if (field == 'name') {
      this.setState({
        name: text,
      });
    } else if (field == 'email') {
      this.setState({
        email: text,
      });
    } else if (field == 'phonenumber') {
      this.setState({
        phonenumber: text,
      });
    } else if (field == 'message') {
      this.setState({
        message: text,
      });
    }
  }
  submit() {
    let collection = {};
    (collection.name = this.state.name),
      (collection.email = this.state.email),
      (collection.phonenumber = this.state.phonenumber),
      (collection.enquirytype = this.state.enquirytype),
      (collection.message = this.state.message);

    var url = 'https://www.whitegrasscapital.com/api/contactmail';

    fetch(url, {
      method: 'POST', // or 'PUT'
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(collection),
    })
      .then(response => response.json())
      .then(response => {
        console.log('Success:', response);
      })
      .catch(error => {
        console.error('Error:', error);
      });
    console.warn(collection);
  }

  render() {
    return (
      <ScrollView style={globalStyles.contactPage}>
        {/* Start Contact Us Section */}

        <View style={globalStyles.ai2Section}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View>
              <Text style={globalStyles.aboutHead}>Contact Us </Text>
            </View>
            <View style={{flex: 1, height: 2, backgroundColor: '#ffc451'}} />
          </View>

          <View>
            <TextInput
              style={globalStyles.input}
              placeholder="Your Name"
              placeholderTextColor="#000"
              keyboardType="ascii-capable"
              onChangeText={text => this.updateValue(text, 'name')}
            />
            <TextInput
              style={globalStyles.input}
              placeholder="Your Email"
              placeholderTextColor="#000"
              keyboardType="email-address"
              onChangeText={text => this.updateValue(text, 'email')}
            />
            <TextInput
              style={globalStyles.input}
              placeholder="Phone Number"
              placeholderTextColor="#000"
              keyboardType="numeric"
              onChangeText={text => this.updateValue(text, 'phonenumber')}
            />
            <View style={globalStyles.Picker}>
              <Picker
                selectedValue={this.state.enquirytype}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({enquirytype: itemValue})
                }>
                <Picker.Item label="Select Type of Enquiry" value="Select" />
                <Picker.Item
                  label="Corporate Enquiry"
                  value="Corporate Enquiry"
                />
                <Picker.Item label="I am investor" value="I am investor" />
                <Picker.Item label="I am buyer" value="I am buyer" />
                <Picker.Item label="I am NRI" value="I am NRI" />
                <Picker.Item
                  label="Business Interest"
                  value="Business Interest"
                />
                <Picker.Item
                  label="Franchise Enquiry"
                  value="Franchise Enquiry"
                />
                <Picker.Item
                  label="Need to list my property"
                  value="Need to list my property"
                />
                <Picker.Item label="I am agent" value="I am agent" />
                <Picker.Item label="General Enquiry" value="General Enquiry" />
                <Picker.Item label="Others" value="Others" />
              </Picker>
            </View>

            <TextInput
              multiline={true}
              numberOfLines={10}
              style={{
                height: 110,
                textAlignVertical: 'top',
                marginBottom: 20,
                paddingLeft: 15,
                fontSize: 16,
                backgroundColor: '#D3D3D3',
                borderBottomColor: 'black',
                borderBottomWidth: 2,
              }}
              placeholder="Your Message"
              placeholderTextColor="#000"
              keyboardType="text"
              onChangeText={text => this.updateValue(text, 'message')}
            />
          </View>

          <TouchableOpacity onPress={() => this.submit()}>
            <Text style={globalStyles.getInTouchButton}>Send Message</Text>
          </TouchableOpacity>
        </View>

        {/* End Contact Us Section */}
      </ScrollView>
    );
  }
}
