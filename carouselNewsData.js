const data = [
  {
    title: ' Our new Product & Service Announcement! ',
    body: 'Leasing & Renting Services for Corporates and its Employees! Click the link and start Browsing for current and upcoming options!                             ',
    imgUrl:
      'https://www.whitegrasscapital.com/admin/uploads/Real_Estate_Buy.png',
    date: '2022-08-10',
  },
  {
    title:
      ' Elan Group buys 40 acres of land from Indiabulls Real Estate for Rs 580 crore',
    body: 'The group said that of the 40 acres, 30 acres are meant for residential development and 10 acres for commercial spaces',
    imgUrl:
      'https://www.whitegrasscapital.com/admin/uploads/Real_Estate_News.jpeg',
    date: '2022-08-10',
  },
];

export default data;
