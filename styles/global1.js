import {faBlackberry} from '@fortawesome/free-brands-svg-icons';
import {withTheme} from '@rneui/themed';
import {StyleSheet} from 'react-native';

export const globalStyles1 = StyleSheet.create({
  logoText1: {
    color: '#000000',
    fontSize: 50,
    marginTop: 30,
    marginBottom: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    fontFamily: 'OpenSans-BoldItalic',
  },
});
