const data = [
  {
    title: 'Happy Customer',
    body: 'Onboarding was seamless & paperless. Legal documents was nicely done. Professional approach. Looking forward.',
  },
  {
    title: 'Happy Customer',
    body: 'Before I used to face lot of uncertainties. White Grass Capital has removed all risk from that, with my recent transactions with them. Every stage was very easy and transparent.',
  },
];

export default data;
